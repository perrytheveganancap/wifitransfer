#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pyotherside, os, threading, socket, random, string
from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer

from zeroconf import ServiceInfo, Zeroconf

server = None

def get_writable_dir():
    APP_ID = os.environ.get("APP_ID", "wifitransfer.aloysliska_ignored")
    APP_PKGNAME = APP_ID.split('_')[0]
    XDG = os.environ.get('XDG_DATA_HOME')
    if not XDG:
        my_writable_dir = "."
    else:
        if not XDG.endswith("/"): XDG = XDG + "/"
        my_writable_dir = XDG + APP_PKGNAME
    HOME = os.path.expanduser("~")
    if not HOME.endswith("/"): HOME = HOME + "/"

    # try writing to $HOME; if it works, make that our folder
    try:
        test_home_file = os.path.join(HOME, "wifitransfer.tmp")
        fp = open(test_home_file, "w")
        fp.write("can we write to $HOME?")
        fp.close()
        os.unlink(test_home_file)
        my_writable_dir = HOME
        my_writable_dir_short = "all"
    except:
        my_writable_dir_short = os.path.abspath(my_writable_dir).replace(HOME, "")

    if not os.path.isdir(my_writable_dir):
        os.mkdir(my_writable_dir)
    return my_writable_dir, my_writable_dir_short

class EventFiringFTPHandler(FTPHandler):
    def on_connect(self):
        pyotherside.send("user_connected", self.remote_ip, self.remote_port)

    def on_disconnect(self):
        pyotherside.send("user_disconnected")

    def on_file_received(self, ffn):
        # do something when a file has been received
        pyotherside.send("files", os.path.split(ffn)[1], ffn)

    def on_incomplete_file_received(self, file):
        # remove partially uploaded files
        import os
        os.remove(file)

class FTPd(threading.Thread):
    """A threaded FTP server used for running tests.
    This is basically a modified version of the FTPServer class which
    wraps the polling loop into a thread.
    The instance returned can be used to start(), stop() and
    eventually re-start() the server.
    """
    handler = EventFiringFTPHandler
    server_class = FTPServer

    def __init__(self, addr=None):
        threading.Thread.__init__(self)
        self.__serving = False
        self.__stopped = False
        self.__lock = threading.Lock()
        self.__flag = threading.Event()
        if addr is None:
            addr = ("", 0)

        my_writable_dir, my_writable_dir_short = get_writable_dir()

        password = "".join([random.choice(string.ascii_lowercase) for i in range(5)])
        authorizer = DummyAuthorizer()
        authorizer.add_user("ubuntu", password, my_writable_dir, perm="elradfmw")

        self.handler.authorizer = authorizer
        # lower buffer sizes = more "loops" while transfering data
        # = less false positives
        self.handler.dtp_handler.ac_in_buffer_size = 4096
        self.handler.dtp_handler.ac_out_buffer_size = 4096
        self.server = self.server_class(addr, self.handler)
        self.host, self.port = self.server.socket.getsockname()[:2]
        host_ip = [(s.connect(('8.8.8.8', 80)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]
        self.host_ip = host_ip
        self.password = password
        pyotherside.send("started", host_ip, self.port, my_writable_dir_short, password)

    def __repr__(self):
        status = [self.__class__.__module__ + "." + self.__class__.__name__]
        if self.__serving:
            status.append('active')
        else:
            status.append('inactive')
        status.append('%s:%s' % self.server.socket.getsockname()[:2])
        return '<%s at %#x>' % (' '.join(status), id(self))

    @property
    def running(self):
        return self.__serving

    def start(self, timeout=0.001):
        """Start serving until an explicit stop() request.
        Polls for shutdown every 'timeout' seconds.
        """
        if self.__serving:
            raise RuntimeError("Server already started")
        if self.__stopped:
            # ensure the server can be started again
            FTPd.__init__(self, self.server.socket.getsockname(), self.handler)
        self.__timeout = timeout
        threading.Thread.start(self)
        self.__flag.wait()
        self.zeroconf_info = ServiceInfo("_ftp._tcp.local.",
                           "WifiTransfer on Ubuntu phone._ftp._tcp.local.",
                           socket.inet_aton(self.host_ip), self.port, 0, 0, {"u":"ubuntu"})
        self.zeroconf = Zeroconf()
        self.zeroconf.register_service(self.zeroconf_info)

    def run(self):
        self.__serving = True
        self.__flag.set()
        while self.__serving:
            self.__lock.acquire()
            self.server.serve_forever(timeout=self.__timeout, blocking=False)
            self.__lock.release()
        self.server.close_all()

    def stop(self):
        """Stop serving (also disconnecting all currently connected
        clients) by telling the serve_forever() loop to stop and
        waits until it does.
        """
        if not self.__serving:
            raise RuntimeError("Server not started yet")
        self.__serving = False
        self.__stopped = True
        self.join(timeout=3)
        #if threading.activeCount() > 1:
        #    warn("test FTP server thread is still running")
        self.zeroconf.unregister_service(self.zeroconf_info)
        self.zeroconf.close()

def start():
    global server
    server = FTPd()
    pyotherside.send("calling-start")
    server.start()

def stop():
    pyotherside.send("stopping?")
    print("IN PYTHON, before close")
    server.stop()
    print("IN PYTHON, after close")
    pyotherside.send("stopped")

def getfolder():
    my_writable_dir, my_writable_dir_short = get_writable_dir()
    pyotherside.send("writeabledir", my_writable_dir, my_writable_dir_short)
