import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

import Lomiri.Components.ListItems 1.3 as ListItem
import Lomiri.Content 1.1
import Lomiri.Components.Popups 1.3

import Qt.labs.folderlistmodel 2.12
//import QtSystemInfo 5.0     // TODO used for ScreenSaver but what for?

/*!
    \brief MainView with a Label and Button elements.
*/

MainView {
    id: root
    // objectName for functional testing purposes (autopilot-qt5)
    objectName: 'mainView'

    // Note! applicationName needs to match the "name" field of the click manifest
    applicationName: 'wifitransfer.aloysliska'

    /*
     This property enables the application to change orientation
     when the device is rotated. The default is false.
    */
    automaticOrientation: false

    width: units.gu(40)
    height: units.gu(71)
    headerColor: "#343C60"
    backgroundColor: "#6A69A2"
    footerColor: "#8896D5"

    //ScreenSaver { screenSaverEnabled: btn.state == "started" }    // TODO: to keep or suppress?

    Tabs {
        id: tabs

        Tab {
            id: wifitransferTab
            title: "WifiTransfer"
            page: Page {
                id: wifitransferPage
                title: "WifiTransfer"
                Flickable {
                    anchors.fill: parent
                    contentHeight: column.height

                    Column {
                        id: column
                        spacing: units.gu(1)
                        anchors {
                            margins: units.gu(2)
                            horizontalCenter: parent.horizontalCenter
                        }
                        width: parent.width - units.gu(4)

                        Row {
                            id: browserow
                            width: parent.width
                            spacing: units.gu(1)
                            Image {
                                height: lblbrowse * 0.8
                                fillMode: Image.PreserveAspectFit
                                source: Qt.resolvedUrl("../assets/whitearrow.png")
                            }
                            Label {
                                id: lblbrowse
                                text: i18n.tr("Browse your files here")
                                anchors.verticalCenter: parent.verticalCenter
                            }
                        }

                        Label {
                            id: spacer
                            text: " "
                        }


                        Button {
                            id: btn
                            objectName: "button"
                            width: parent.width
                            gradient: LomiriColors.orangeGradient

                            states: [
                                State {
                                    name: "started"
                                    PropertyChanges { target: btn; text: i18n.tr("Turn off WifiTransfer"); enabled: true }
                                },
                                State {
                                    name: "starting"
                                    PropertyChanges { target: btn; text: i18n.tr("(starting)"); enabled: false }
                                },
                                State {
                                    name: "stopping"
                                    PropertyChanges { target: btn; text: i18n.tr("(stopping)"); enabled: false }
                                },
                                State {
                                    name: "stopped"
                                    PropertyChanges { target: btn; text: i18n.tr("Turn on WifiTransfer"); enabled: true }
                                }
                            ]
                            text: i18n.tr("(initialising)")

                            onClicked: {
                                if (btn.state == "stopped") {
                                    btn.state = "starting";
                                    py.call("fserver.start", function() {
                                        console.log("fserver.start returns");
                                    });
                                } else if (btn.state == "started") {
                                    btn.state = "stopping";
                                    py.call("fserver.stop", function() {
                                        console.log("fserver.stop returns");
                                    });
                                }
                                browserow.visible = false;
                            }
                            Component.onCompleted: { state = "stopped"; }
                        }

                        Rectangle {
                            id: lblconnected
                            width: parent.width
                            property bool showing: false
                            height: showing ? lblconnectedtext.height * 4 : 0
                            clip: true
                            Behavior on height { PropertyAnimation {} }
                            color: Qt.rgba(255,255,255,0.2)

                            Label {
                                id: lblconnectedtext
                                text: i18n.tr("Connected")
                                anchors.verticalCenter: parent.verticalCenter
                                horizontalAlignment: Text.AlignHCenter
                                width: parent.width
                                fontSize: "large"
                            }
                        }

                        Row {
                            id: desc_select
                            spacing: units.gu(1)
                            width: parent.width
                            visible: btn.state == "started"

                            Button {
                                id: desc_zeroconf
                                text: "Ubuntu"
                                width: (parent.width - (2*desc_select.spacing)) / 3
                                property bool btactive: true
                                gradient: btactive ? LomiriColors.orangeGradient : LomiriColors.greyGradient
                                onClicked: {
                                    if (!btactive) {
                                        btactive = true;
                                        desc_nautilus.btactive = false;
                                        desc_text.btactive = false;
                                    }
                                }
                            }
                            Button {
                                id: desc_nautilus
                                // TRANSLATORS: this is "File Manager", but it MUST be 8 chars or fewer to fit on its button
                                text: i18n.tr("File Mgr")
                                width: (parent.width - (2*desc_select.spacing)) / 3
                                property bool btactive: false
                                gradient: btactive ? LomiriColors.orangeGradient : LomiriColors.greyGradient
                                onClicked: {
                                    if (!btactive) {
                                        btactive = true;
                                        desc_zeroconf.btactive = false;
                                        desc_text.btactive = false;
                                    }
                                }
                            }
                            Button {
                                id: desc_text
                                text: i18n.tr("Details")
                                width: (parent.width - (2*desc_select.spacing)) / 3
                                property bool btactive: false
                                gradient: btactive ? LomiriColors.orangeGradient : LomiriColors.greyGradient
                                onClicked: {
                                    if (!btactive) {
                                        btactive = true;
                                        desc_nautilus.btactive = false;
                                        desc_zeroconf.btactive = false;
                                    }
                                }
                            }
                        }

                        Label {
                            id: lbl
                            anchors.horizontalCenter: parent.horizontalCenter
                            property string ip
                            property string port
                            property string folder
                            property string password
                            horizontalAlignment: Text.AlignHCenter

                            visible: btn.state == "started" && desc_text.btactive
                            text: {
                                // TRANSLATORS: do not translate IPADDRESS or PORTNUMBER; leave these words as English
                                var firsttext = i18n.tr("Connect your FTP app to IPADDRESS on port PORTNUMBER");
                                var outtext1 = "<p>" + firsttext.replace(
                                    "IPADDRESS", '<br><font size="7">' + ip + '</font><br>').replace(
                                    "PORTNUMBER", '<br><font size="7">' + port + '</font>'
                                ) + "</p>";
                                var outtext2 = '<p><font size="5">' +
                                        i18n.tr("Username") +
                                        ': <strong>ubuntu</strong><br>' +
                                        i18n.tr("Password") +
                                        ': <strong>' + password + '</strong></font></p>';
                                var outtext3;
                                if (folder == "all") {
                                    // TRANSLATORS: please keep "all your files" highlighted with HTML <b> tags
                                    var before3 = i18n.tr("Access to <b>all your files</b>");
                                    outtext3 = "<p>" + before3.replace("<b>", '<b><a href="#">').replace("</b>", "</a></b>") + "</p>";
                                } else {
                                    outtext3 = '<p><br>' +
                                        i18n.tr("Writing to folder") +
                                        '<br><b><a href="#">' + folder + "</a></b></p>";
                                }
                                return outtext1 + outtext2 + outtext3;
                            }

                            onLinkActivated: tabs.selectedTabIndex = 1
                            linkColor: "#c1c101"
                            wrapMode: Text.Wrap
                            textFormat: Text.RichText
                        }

                        Label {
                            id: lbl_nautilus
                            anchors.horizontalCenter: parent.horizontalCenter
                            horizontalAlignment: Text.AlignHCenter

                            visible: btn.state == "started" && desc_nautilus.btactive
                            text: {
                                var outtext2 = '<p><font size="5">' +
                                        i18n.tr("Password") +
                                        ': <strong>' + lbl.password + '</strong></font></p>';
                                var outtext3;
                                if (lbl.folder == "all") {
                                    // TRANSLATORS: please keep "all your files" highlighted with HTML <b> tags
                                    var before3 = i18n.tr("Access to <b>all your files</b>");
                                    outtext3 = "<p>" + before3.replace("<b>", '<b><a href="#">').replace("</b>", "</a></b>") + "</p>";
                                } else {
                                    outtext3 = '<p><br>' +
                                        i18n.tr("Writing to folder") +
                                        '<br><b><a href="#">' + lbl.folder + "</a></b></p>";
                                }
                                return outtext2 + outtext3;
                            }
                            onLinkActivated: tabs.selectedTabIndex = 1
                            linkColor: "#c1c101"
                            wrapMode: Text.Wrap
                            textFormat: Text.RichText
                        }

                        Item {
                            width: parent.width - units.gu(1)
                            height: units.gu(35)
                            visible: btn.state == "started" && desc_nautilus.btactive
                            anchors.horizontalCenter: parent.horizontalCenter
                            Image {
                                id: cts
                                anchors.fill: parent
                                fillMode: Image.PreserveAspectFit
                                source: Qt.resolvedUrl("../assets/nautilus-cts.png")
                            }
                            Text {
                                x: (parent.width / 2) - (cts.paintedWidth * 0.43)
                                y: (parent.height/ 2) - (cts.paintedHeight * 0.23)
                                font.pixelSize: cts.paintedHeight / 20
                                text: "ftp://ubuntu@" + lbl.ip + ":" + lbl.port + "/"
                            }
                        }

                        Label {
                            id: lbl_zeroconf
                            anchors.horizontalCenter: parent.horizontalCenter
                            horizontalAlignment: Text.AlignHCenter

                            visible: btn.state == "started" && desc_zeroconf.btactive
                            text: {
                                var outtext2 = '<p><font size="5">' +
                                        i18n.tr("Password") +
                                        ': <strong>' + lbl.password + '</strong></font></p>';
                                var outtext3;
                                if (lbl.folder == "all") {
                                    // TRANSLATORS: please keep "all your files" highlighted with HTML <b> tags
                                    var before3 = i18n.tr("Access to <b>all your files</b>");
                                    outtext3 = "<p>" + before3.replace("<b>", '<b><a href="#">').replace("</b>", "</a></b>") + "</p>";
                                } else {
                                    outtext3 = '<p><br>' +
                                        i18n.tr("Writing to folder") +
                                        '<br><b><a href="#">' + lbl.folder + "</a></b></p>";
                                }
                                return outtext2 + outtext3;
                            }
                            onLinkActivated: tabs.selectedTabIndex = 1
                            linkColor: "#c1c101"
                            wrapMode: Text.Wrap
                            textFormat: Text.RichText
                        }

                        Item {
                            width: parent.width - units.gu(1)
                            height: units.gu(35)
                            visible: btn.state == "started" && desc_zeroconf.btactive
                            anchors.horizontalCenter: parent.horizontalCenter
                            Image {
                                id: zcn
                                anchors.fill: parent
                                fillMode: Image.PreserveAspectFit
                                source: Qt.resolvedUrl("../assets/nautilus-zeroconf.png")
                            }
                        }


                        Python {
                            id: py
                            Component.onCompleted: {
                                // Add the directory of this .qml file to the search path
                                addImportPath(Qt.resolvedUrl('../src'));
                                importModule('fserver', function () {
                                    console.log("Python module loaded");
                                });
                            }
                            onError: console.log('Python error: ' + traceback)
                            onReceived: {
                                console.log("got python data", data);
                                if (Array.isArray(data) && data[0] == "started") {
                                    btn.state = "started";
                                    lbl.ip = data[1];
                                    lbl.port = data[2];
                                    lbl.folder = data[3];
                                    lbl.password = data[4];
                                } else if (data == "stopped") {
                                    btn.state = "stopped";
                                } else if (data == "files_use_file_manager") {
                                    console.log("unlocked, fail")
                                } else if (Array.isArray(data) && data[0] == "files") {
                                    filesModel.append({fn: data[1], ffn: data[2]})
                                } else if (Array.isArray(data) && data[0] == "writeabledir") {
                                    filesModel.rootFolder = data[1];
                                    filesModel.folder = data[1];
                                } else if (Array.isArray(data) && data[0] == "user_connected") {
                                    lblconnected.showing = true;
                                } else if (Array.isArray(data) && data[0] == "user_disconnected") {
                                    lblconnected.showing = false;
                                }
                            }
                        }
                        Label {
                            anchors.right: parent.right
                            anchors.topMargin: units.gu(5)
                            anchors.rightMargin: units.gu(1)
                            text: i18n.tr('this is an <a href="http://www.kryogenix.org">sil</a> thing')
                            onLinkActivated: Qt.openUrlExternally("http://www.kryogenix.org")
                            fontSize: "small"
                            linkColor: "#c1c101"
                        }

                        Label {
                            id: spacer2
                            text: " "
                        }
                    }
                }
            }
        }

        Tab {
            id: filesTab
            FolderListModel {
                id: filesModel
                sortField: FolderListModel.Name
                showDirsFirst: true
                showDotAndDotDot: true
            }

            title: i18n.tr("Files")
            page: Page {
                id: filesPage
                title: i18n.tr("Files")
                ListView {
                    id: filesView
                    model: filesModel
                    clip: true
                    anchors.fill: parent
                    delegate: ListItem.Standard {
                        id: fileDelegate
                        iconName: fileName == ".." ? "up" : (fileIsDir ? "folder" : "share")
                        iconFrame: false
                        text: fileName == ".." ? "up" : fileName
                        visible: fileName != "."
                        onClicked: {
                            if (fileIsDir) {
                                if (fileName == "..") {
                                    var parts = filesModel.folder.toString().split("/");
                                    filesModel.folder = parts.slice(0, parts.length - 1).join("/")
                                } else {
                                    filesModel.folder = filesModel.folder + "/" + fileName
                                }
                            } else {
                                console.log("content hub file", filesModel.folder + "/" + fileName);
                                cpp.fileurl = filesModel.folder + "/" + fileName;
                                cpp.visible = true;
                            }
                        }
                    }
                }
            }
        }

        onSelectedTabChanged: {
            if (selectedTab == filesTab || selectedTab == aboutTab) {
                if (!filesModel.rootfolder) {
                    py.call("fserver.getfolder")
                }
                browserow.visible = false;
            }
        }

        Tab {
            id: aboutTab

            title: i18n.tr("About")
            page: Page {
                title: i18n.tr("About")
                Flickable {
                    anchors.fill: parent
                    contentHeight: column2.height

                    Column {
                        id: column2
                        spacing: units.gu(1)
                        anchors {
                            margins: units.gu(2)
                            horizontalCenter: parent.horizontalCenter
                        }
                        width: parent.width - units.gu(4)

                        Label {
                            id: spacer3
                            text: " "
                        }
                        Label {
                            width: parent.width - units.gu(2)
                            anchors.horizontalCenter: parent.horizontalCenter
                            text: i18n.tr("<p>Many thanks to the checkbox project for providing an example of how to use Python in an Ubuntu SDK app, mzanetti for content hub help, and Dustin MacDonald for the icon.</p>") +
                                  i18n.tr("<p>Many thanks also to pyotherside from Thomas Perl, pyzeroconf from William McBrine, and pyftpdlib from Giampaolo Rodolà without which this app couldn’t exist.</p>") +
                                  i18n.tr('<p>WifiTransfer is an <a href="http://www.kryogenix.org">sil</a> thing.</p>')
                            onLinkActivated: Qt.openUrlExternally("http://www.kryogenix.org")
                            linkColor: "#c1c101"
                            wrapMode: Text.Wrap
                        }
                    }
                }

            }
        }
    }

    ContentItem {
        id: exportItem
        name: i18n.tr("Transferred file")
    }

    ContentPeerPicker {
        id: cpp
        property string fileurl
        anchors.fill: parent
        visible: false
        handler: ContentHandler.Destination
        contentType: ContentType.All
        onPeerSelected: {
            var transfer = peer.request();
            var items = new Array();
            exportItem.url = cpp.fileurl;
            items.push(exportItem);
            transfer.items = items;
            transfer.state = ContentTransfer.Charged;
            cpp.visible = false;
        }
        onCancelPressed: cpp.visible = false;
    }
}

