
# WifiTransfer
WifiTransfer is an application to transfer files to and from your Ubuntu Touch phone with FTP.

A simple FTP server for Ubuntu Touch devices; start it and then connect to your device from your normal FTP application on your computer (the file manager, Filezilla, or any other) using the connection details provided.


# Author and Contributor
 - Updated by **Aloys Liska** in 2024 for Focal (Ubuntu 20.04)
 - Original Author: **Stuart Langridge** in 2015-2017. See https://launchpad.net/wifitransfer


# Licence
MIT / X / Expat Licence


# Original *readme.txt* content
where did you get the python stuff from, Langridge?

Install checkbox on your phone, then look in com.ubuntu.checkbox/current/lib in the click folder in /opt, which has all the python stuff in and copy it all into your app. Remove lib/py, because that's checkbox's dependencies.

TODO

* Translations (done on LP, but not integrated back into app yet)
* keep screen turned on while server is running and app is foreground
* keep server running while app is in background (not yet possible)
* change design to look like https://dribbble.com/shots/1878610-Monochromatic-calendar
* openstore version
